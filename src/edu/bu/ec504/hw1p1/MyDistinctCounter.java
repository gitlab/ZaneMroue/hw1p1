package edu.bu.ec504.hw1p1;
import java.nio.charset.StandardCharsets;
import java.util.BitSet;



import static java.lang.Math.ceil;

/**
 * This is a trivial modification!
 */
public class MyDistinctCounter extends DistinctCounter {
  /**
   * @inheritDoc
   */
  public MyDistinctCounter(int memBits) {
    super(memBits);
  }
  /**
   * @inheritDoc
   */
  @Override
  public void saw(String newElement) {
    int hash = newElement.hashCode();
    hash =  Math.abs(hash);
    int pos = Integer.numberOfTrailingZeros(hash);
    //System.out.println(pos);
    if (pos < mem.len) {
      mem.set(pos);
    }
  }


  /**
   * @inheritDoc
   */
  @Override
  public Integer numDistinct() {
    int bitsSet = 0;
    for (int i = 0; i < mem.len; i++) {
      if (mem.get(i)) {
        bitsSet++;
      }
    }
    if (bitsSet == 0) {
      return 0;
    }

    double correctionFactor = calculateCorrectionFactor(bitsSet, mem.len);
    double res;
    int INTres;
    int R = -1;
    for (int i = 0; i < mem.len; i++){
      if (!this.mem.get(i)){
        R = i;
        break;
      }
    }
    if (R == -1){
      res = Math.pow(2, mem.len) * correctionFactor;
    }else{
      res = (Math.pow(2, R) *0.77351);
    }
    res = Math.ceil(res);
    INTres = (int) res;
    return INTres;
  }

  private double calculateCorrectionFactor(int bitsSet, int totalBits) {
    double saturationLevel = (double) bitsSet / totalBits;
    double adjus = 0.5;
    if (saturationLevel > 0.95) {
      adjus = 2.95;
    } else if (saturationLevel > 0.5) {
      adjus = 1.4;
    }
    double logAdjustment = Math.log(totalBits) /Math.log(2);
    return adjus *logAdjustment;
  }

  // NESTED CLASSES
  /**
   * A runtime exception indicating that a method has not yet been implemented.
   */
  public static class NotYetImplemented extends RuntimeException {
  }

}
